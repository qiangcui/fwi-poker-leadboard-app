Note: This project was developed by Qiang (Paul) Cui.

## How To Run Application?

1. Please pull the repository by typing ```git clone https://gitlab.com/qiangcui/fwi-poker-leadboard-app.git``` in the terminal.

2. Make sure you installed docker and docker-compose in your work station.

3. Please open the terminal and type ```docker login``` and put your docker hub username and password correctly and log in successfully.

4. If your work station is PC, please type ```ipconfig``` in cmd terminal and find out the ip address of your work station.

5. If your work station is Mac/Linux, please types ```ifconfig``` in terminal and find out the ip address of your work station.

6. Go to client/package.json file and open it.

7. Change the old ip address in the proxy section with your ip address - ```"proxy": "http://{Put your ip address here}:8080".```

8. Open the terminal and jump to root directory where the docker-compose file is located at.

9. Run ```docker-compose up --build``` in terminal.

10. In Chrome Browser, type ```localhost:3000``` and see the home page.

11. You can see the list and feel free to add, delete, and update the porker data.

12. In Chrome Browser, type ```localhost:8080/docs``` and you can see the swagger docs.

