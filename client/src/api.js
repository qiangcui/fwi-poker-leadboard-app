import { API_SERVER } from './constants';

export const getPokers = () => (
    API_SERVER.get('/poker').then(res => res.data).catch(error => error.response)
);

export const addPoker = (new_poker) => (
    API_SERVER.post('/poker', new_poker).then(res => res.data).catch(error => error.response)
);

export const updatePoker = (id, modified_data) => (
    API_SERVER.put(`/poker/${id}`, modified_data).then(res => res.data).catch(error => error.response)
);

export const deletePoker = (id) => (
    API_SERVER.delete(`/poker/${id}`).then(res => res.data).catch(error => error.response)
);
