import axios from 'axios';

export const UPDATE_POKER_DATA = "poker:updatePokerData";
export const API_SERVER = axios.create();