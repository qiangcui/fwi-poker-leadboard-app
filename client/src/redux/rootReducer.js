import { combineReducers } from "redux";
import PokerDataReducer from './reducers/pokerDataReducer';
import { reducer as form } from "redux-form";

export default combineReducers({
   poker_data: PokerDataReducer,
   form: form
});