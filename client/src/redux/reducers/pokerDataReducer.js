import {
    UPDATE_POKER_DATA,
} from "../../constants";


const initialState = {
    poker_data: []
};

const PokerDataReducer = (state = initialState, action) => {
    switch (action.type) {
        case UPDATE_POKER_DATA:
            return {
                ...state,
                poker_data: [...action.payload.poker_data]
            };
        default:
            return state;
    }
};

export default PokerDataReducer;