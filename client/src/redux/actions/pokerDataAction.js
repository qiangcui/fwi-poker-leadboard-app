import {
    UPDATE_POKER_DATA,
} from "../../constants";
import { getPokers, addPoker, updatePoker, deletePoker } from "../../api";

export const updatePokerData = (new_poker_data) => {
    return {
        type: UPDATE_POKER_DATA,
        payload: {
            poker_data: new_poker_data
        }
    }
};

export const getPokersList = () => {
    return dispatch => {
        getPokers()
            .then(
                res => {
                    dispatch(updatePokerData(res));
                }
            )
            .catch(
                error => {
                    console.log(error);
                }
            )
    }
};

export const addPokerItem = (new_poker) => {
    return dispatch => {

        addPoker(new_poker)
            .then(
                () => {
                    dispatch(getPokersList());
                }
            )
            .catch(
                error => {
                    console.log(error);
                }
            )
    }
};

export const updatePokerItem = (id, modified_data) => {
    return dispatch => {
        updatePoker(id, modified_data)
            .then(
                () => {
                    dispatch(getPokersList());
                }
            )
            .catch(
                error => {
                    console.log(error);
                }
            )
    }
};

export function deletePokerItem(id) {
    return dispatch => {
        deletePoker(id)
            .then(
                () => {
                    dispatch(getPokersList());
                }
            )
            .catch(
                error => {
                    console.log(error);
                }
            )
    }
}
