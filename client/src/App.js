import React, { Component } from 'react';
import { Field } from "redux-form";
import Table from "react-table";
import * as BS from "react-bootstrap";
import FormProvider from "./components/table/FormProvider";
import { avatarColumnProps } from "./components/table/AvatarCell";
import ActionsCell from "./components/table/ActionsCell";
import HighlightCell from "./components/table/HightlightCell";
import GridFilters from "./components/table/GridFilters";
import { connect } from "react-redux";
import { getPokersList, addPokerItem, updatePokerItem, deletePokerItem } from "./redux/actions/pokerDataAction";
import AddPokerMainComponent from './components/AddPokerMainComponent';
import { Container, Header } from 'semantic-ui-react'

class App extends Component {
    constructor(props) {
        super(props);

        this.state = {
            modalShow: false,
            editing: null
        };
    }

    componentDidMount(){
        this.props.getPokersList();
    }

    editableComponent = ({ input, editing, value, ...rest }) => {
        console.log('input', input, value, rest);
        const Component = editing ? BS.FormControl : BS.FormControl.Static;
        const children =
            (!editing && <HighlightCell value={value} {...rest} />) || undefined;
        return <Component {...input} {...rest} children={children} />;
    };

    editableColumnProps = {
        ...GridFilters,
        Cell: props => {
            const editing = this.state.editing === props.original;
            const fieldProps = {
                component: this.editableComponent,
                editing,
                props
            };
            return <Field style={{ textAlign: 'center' }} name={props.column.id} {...fieldProps} />;
        }
    };

    getActionProps = (gridState, rowProps) =>
        (rowProps && {
            mode: this.state.editing === rowProps.original ? "edit" : "view",
            actions: {
                onEdit: () => this.setState({ editing: rowProps.original }),
                onDelete: (index) => {
                    let id = this.props.poker_data[index]['id'];
                    this.props.deletePokerItem(id);
                    this.setState({ editing: null })},
                onCancel: () => this.setState({ editing: null })
            }
        }) ||
        {};

    columns = [
        { Header: "Ranking", accessor: "ranking", ...this.editableColumnProps },
        { Header: "", accessor: "picture_url", ...avatarColumnProps },
        { Header: "Name", accessor: "name", ...this.editableColumnProps },
        { Header: "Winnings", accessor: "winning", ...this.editableColumnProps },
        { Header: "Native Of", accessor: "country" },
        {
            Header: "",
            maxWidth: 500,
            filterable: false,
            getProps: this.getActionProps,
            Cell: ActionsCell
        }
    ];

    handleSubmit = values => {
        this.props.updatePokerItem(values['id'], values);
    };

    handleUpload = (new_poker) => {
        this.props.addPokerItem(new_poker);
    };

    render() {
        return (
            <React.Fragment>
                <Container fluid style={{ margin: 50, width: '65%' }}>
                    <Header as='h2'>ALL-TIME TOURNAMENT EARNINGS</Header>
                    <AddPokerMainComponent handleUpload={this.handleUpload}/>
                    <FormProvider
                        form="inline"
                        onSubmit={this.handleSubmit}
                        onSubmitSuccess={() => this.setState({ editing: null })}
                        initialValues={this.state.editing}
                        enableReinitialize
                    >
                        {formProps => {
                            return (
                                <form onSubmit={formProps.handleSubmit}>
                                    {this.props.poker_data && <Table
                                        columns={this.columns}
                                        data={this.props.poker_data}
                                        defaultPageSize={20}
                                    />}
                                </form>
                            );
                        }}
                    </FormProvider>
                </Container>
            </React.Fragment>
        );
    }
}

const mapStateToProps = state => ({
  poker_data: state.poker_data.poker_data,
  isLoading: state.poker_data.isLoading,
  error: state.poker_data.error
});

const mapActionsToProps = {
    getPokersList: getPokersList,
    addPokerItem: addPokerItem,
    updatePokerItem: updatePokerItem,
    deletePokerItem: deletePokerItem
};

export default connect(
    mapStateToProps, mapActionsToProps
)(App);
