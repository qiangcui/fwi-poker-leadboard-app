import React, { Component } from 'react'
import { Button, Form } from 'semantic-ui-react'

class AddPokerFormComponent extends Component {
    constructor(props) {
        super(props);

        this.state = {
            name: '',
            ranking: 0,
            winning: 0,
            country: '',
            picture_url: '',
            options: [
                { key: '1', text: 'USA', value: 'USA' },
                { key: '2', text: 'GBR', value: 'GBR' },
                { key: '3', text: 'DEU', value: 'DEU' },
                { key: '4', text: 'CAN', value: 'CAN' },
            ]
        };
    }

    handleUpload = (e) => {
        this.setState({
            name: e.target.querySelector('input[name="name"]').value,
            ranking: e.target.querySelector('input[name="ranking"]').value,
            winning: e.target.querySelector('input[name="winning"]').value,
            country: e.target.querySelector('div[name="country"] .text').textContent,
            picture_url: e.target.querySelector('input[name="picture_url"]').value
        }, () => {
            this.props.handleUpload(this.state);
            this.props.handleClose();
        });
    };

    render() {
        return (
            <Form onSubmit={this.handleUpload}>
                <Form.Field required>
                    <label>Name</label>
                    <input placeholder='Player Name' name="name"/>
                </Form.Field>
                <Form.Field required>
                    <label>Ranking (Please Insert Number)</label>
                    <input placeholder='Ranking' name="ranking"/>
                </Form.Field>
                <Form.Field required>
                    <label>Pictures</label>
                    <input placeholder='Picture URL' name="picture_url"/>
                </Form.Field>
                <Form.Field required>
                    <label>Winnings (Please Insert Number)</label>
                    <input placeholder='Winnings' name="winning"/>
                </Form.Field>
                <Form.Field required>
                    <label>Native Of</label>
                    <Form.Select options={this.state.options} placeholder='Country' name="country" />
                </Form.Field>
                <Button type='submit'>Submit</Button>
            </Form>
        )};
}

export default AddPokerFormComponent