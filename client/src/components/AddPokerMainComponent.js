import React, { Component } from 'react'
import { Button, Modal } from 'semantic-ui-react'
import AddPokerFormComponent from './AddPokerFormComponent'

const modalStyle = {
    width: '60%',
    height: '70%',
    margin: '100px 320px',
};

class AddPokerMainComponent extends Component {
    constructor(props) {
        super(props);
        this.state = { modalOpen: false };
    }

    handleOpen = () => this.setState({ modalOpen: true });

    handleClose = () => this.setState({ modalOpen: false });

    render() {
        return (
            <Modal
                style={modalStyle}
                trigger={<Button primary style={{marginTop: 30, marginBottom: 30}} onClick={this.handleOpen}>Show Modal</Button>}
                open={this.state.modalOpen}
                onClose={this.handleClose}
            >
                <Modal.Content>
                    <AddPokerFormComponent handleUpload={this.props.handleUpload} handleClose={this.handleClose}/>
                </Modal.Content>
            </Modal>
        )
    }
}

export default AddPokerMainComponent