import { app } from './app';
import * as http from 'http';
import * as mongoose from 'mongoose';

const PORT = 8080;
const server = http.createServer(app);
const MONGO_URL = "mongodb://mongo:27017/Poker";
server.listen(PORT);
server.on('listening', async () => {
    console.log(`Listening on port ${PORT}`);
    mongoose.connect(MONGO_URL, { useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true});
    mongoose.connection.on('open', () => {
        console.info('Connected to Mongo.');
    });
    mongoose.connection.on('error', (err: any) => {
        console.info(err);
    });
});
