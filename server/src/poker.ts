import * as mongoose from 'mongoose';

interface IPoker {
    _id: string;
    name: string;
    ranking: number;
    picture_url: string;
    winning: number
    country: number
}

interface ICountry {
    _id: string;
    country_id: number,
    country_name: string
}

const PokerSchema = new mongoose.Schema({
    name: String,
    ranking: Number,
    picture_url: String,
    winning: Number,
    country_id: Number
}, {
    versionKey: false
});

const CountrySchema = new mongoose.Schema({
    country_id: Number,
    country_name: String
}, {
    versionKey: false
});

const CountryModel = mongoose.model('country_table', CountrySchema);

const PokerModel = mongoose.model('pokers', PokerSchema);


export { PokerModel, CountryModel, IPoker, ICountry }
