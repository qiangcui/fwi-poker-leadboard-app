import {PokerModel, IPoker, CountryModel} from "./poker";
import { Controller, Route, Get, Post, BodyProp, Put, Delete, Body, SuccessResponse } from "tsoa";

const country_mapping = { "USA": 1, "GBR": 2, "DEU": 3, "CAN":4 };

@Route('/poker')
export class PokerController extends Controller {
    @Get()
    public async getAll(): Promise<IPoker[]> {
        try {
            let items: any =  await PokerModel.aggregate([
                { "$lookup": {
                        "localField": "country_id",
                        "from": "country",
                        "foreignField": "country_id",
                        "as": "country_info"
                    } },
                { "$unwind": "$country_info" },
                { "$project": {
                        "name": 1,
                        "ranking": 1,
                        "picture_url": 1,
                        "winning": 1,
                        "country_info.country_name": 1
                    } }
            ]);

            items = items.map((item) => {
                return {
                    id: item._id,
                    name: item.name,
                    ranking: item.ranking,
                    picture_url: item.picture_url,
                    winning: item.winning,
                    country: item.country_info.country_name
                }
            });
            return items;
        } catch (err) {
            this.setStatus(500);
            console.error('Caught error', err);
        }
    }

    @SuccessResponse('201', 'Created')
    @Post()
    public async create(
        @BodyProp('name') name: string,
        @BodyProp('ranking') ranking: number,
        @BodyProp('picture_url') picture_url: string,
        @BodyProp('winning') winning: number,
        @BodyProp('country') country: string,
    ) : Promise<any> {
        try {
            const item = new PokerModel({
                name: name,
                ranking: ranking,
                picture_url: picture_url,
                winning: winning,
                country_id: country_mapping[country]
            });

            await item.save();
            return Promise.resolve();
        } catch (err){
            this.setStatus(500);
            return Promise.reject();
        }
    }

    @SuccessResponse('201', 'Updated')
    @Put('/{id}')
    public async update(
            id: string,
            @Body() body,
        ) : Promise<any> {
        try {
            body['country_id'] = country_mapping[body.country];
            delete body.country;

            await PokerModel.findByIdAndUpdate({_id: id}, body);
            return Promise.resolve();
        } catch (err){
            this.setStatus(500);
            return Promise.reject();
        }
    }

    @SuccessResponse('201', 'Deleted')
    @Delete('/{id}')
    public async delete(id: string) : Promise<void> {
        try {
            await PokerModel.findByIdAndDelete(id);
            return Promise.resolve();
        } catch (err){
            this.setStatus(500);
            return Promise.reject();
        }
    }
}