#!/usr/bin/env bash
echo "Creating mongo pokers..."
mongo admin --host mongo <<\EOF
    use Poker;
    db.system.js.save(
        {
            _id: "getNextSequence",
            value: function(counterName){
                var updatedCounter = db.counters.findAndModify(
                    {
                        query: { '_id': counterName },
                        update: { $inc: { 'seq': NumberInt(1) } },
                        new: true
                    }
                 );
                 return updatedCounter.seq;
            }
        }
    );
    db.system.js.save(
        {
            _id: "echoFunction",
            value: function (x) {
                return 'echo: ' + x;
            }
        }
    );
    db.loadServerScripts();
    db.pokers.insert(
        [
            {"name" : "Bryn Kenney", "ranking" : 1, "picture_url" : "https://media.cardplayer.com/assets/players/000/054/732/head_shot/large_BrynSQ.JPG", "winning" : 57.3, "country_id" : 1},
            {"name" : "Justin Bonomo","ranking" : 2,"picture_url" : "https://media.cardplayer.com/assets/players/000/009/744/head_shot/large_Bonomo_SQ.jpg","winning" : 48.7,"country_id" : 1},
            {"name" : "Daniel Negreanu","ranking" : 3,"picture_url" : "https://media.cardplayer.com/assets/players/000/001/185/head_shot/large_Daniel_Negreanu_SQ.jpg","winning" : 40.9,"country_id" : 4},
            {"name" : "Dan Smith","ranking" : 4,"picture_url" : "https://media.cardplayer.com/assets/players/000/167/029/head_shot/large_RS24413_Dan_Smith.JPG","winning" : 37.6,"country_id" : 1},
            {"name" : "Erik Seidel", "ranking" : 5, "picture_url" : "https://media.cardplayer.com/assets/players/000/000/735/head_shot/large_medium_Erik_Seidel_3.jpg", "winning" : 35.8, "country_id" : 1},
            {"name" : "David Peters","ranking" : 6,"picture_url" : "https://media.cardplayer.com/assets/players/000/020/666/head_shot/large_medium_David_Peters_1.jpg","winning" : 34.8,"country_id" : 1},
            {"name" : "Stephen Chidwick","ranking" : 7,"picture_url" : "https://media.cardplayer.com/assets/players/000/077/845/head_shot/large_Chidwick_SQ.JPG","winning" : 33.7,"country_id" : 2},
            {"name" : "Fedor Holz","ranking" : 8,"picture_url" : "https://media.cardplayer.com/assets/players/000/251/587/head_shot/large_HolzSQ.jpg","winning" : 33.0,"country_id" : 3},
            {"name" : "Jason Koon","ranking" : 9,"picture_url" : "https://media.cardplayer.com/assets/players/000/178/355/head_shot/large_medium_JasonKoon_Large_.JPG","winning" : 32.0,"country_id" : 1},
            {"name" : "Daniel Colman","ranking" : 10,"picture_url" : "https://media.cardplayer.com/assets/players/000/091/723/head_shot/large_DanColemanSQ.JPG","winning" : 28.7,"country_id" : 1}
        ]
    );
    db.country.insert(
        [
            {"country_id" : 1, "country_name" : "USA"},
            {"country_id" : 2, "country_name" : "GBR"},
            {"country_id" : 3, "country_name" : "DEU"},
            {"country_id" : 4, "country_name" : "CAN"}
        ]
    );
EOF
echo "Mongo pokers created."